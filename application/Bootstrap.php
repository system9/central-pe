<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected $_view;
	public function _initModuleLoaders()
    {
        $this->bootstrap('Frontcontroller');

        $fc = $this->getResource('Frontcontroller');
        $modules = $fc->getControllerDirectory();

        foreach ($modules AS $module => $dir) {
            $moduleName = strtolower($module);
            $moduleName = str_replace(array('-', '.'), ' ', $moduleName);
            $moduleName = ucwords($moduleName);
            $moduleName = str_replace(' ', '', $moduleName);

            $loader = new Zend_Application_Module_Autoloader(array(
                'namespace' => $moduleName,
                'basePath' => realpath($dir . "/../"),
            ));
        }
    }

    protected function _initExcel()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->pushAutoloader(new Pe_Loader_Autoloader_PHPExcel());
    }

    protected function _initView()
    {
        $view = new Zend_View();
        $view->doctype('XHTML1_STRICT');

        $view->headTitle('New Central PE')->setSeparator(' - ');
        $view->headMeta()
                ->appendHttpEquiv('Expires', gmdate('D, d M Y H:i:s', time( ) + 10800) . ' GMT')
                ->appendHttpEquiv('Cache-Control', 'must-revalidate')
                ->appendHttpEquiv('Content-Type', 'application/xhtml+xml; charset=utf-8')
                ->setName('description', 'cmms')
                ->setName('keywords', 'cmms')
                ->setName('robots', 'index, follow');

        $view->headLink()->headLink(array('rel' => 'favicon', 'href'=> '/favicon.ico','type' => 'image/x-icon'), 'PREPEND');

        $view->headLink()->appendStylesheet($view->baseUrl('css/reset.css'));
        $view->headLink()->appendStylesheet($view->baseUrl('css/bootstrap.css'));
        $view->headLink()->appendStylesheet($view->baseUrl('css/bootstrap-responsive.css'));
        $view->headLink()->appendStylesheet($view->baseUrl('css/style.css'));
        $view->headLink()->appendStylesheet($view->baseUrl('css/ie6.css'), 'screen', 'lt IE 7');
		//$view->headLink()->appendStylesheet($view->baseUrl('css/app.css'));
		$view->headLink()->appendStylesheet($view->baseUrl('js/jqplot/jquery.jqplot.min.css'));

        $view->headScript()->appendFile($view->baseUrl('js/jquery.min.js'));
        $view->headScript()->appendFile($view->baseUrl('js/ie6.js'), 'text/javascript',
            array('conditional' => 'lt IE 7'));
		$view->headScript()->appendFile($view->baseUrl('js/validate/jquery.metadata.js'));
        $view->headScript()->appendFile($view->baseUrl('js/validate/jquery.validate.min.js'));
        $view->headScript()->appendFile($view->baseUrl('js/form/jquery.form.js'));
		$view->headScript()->appendFile($view->baseUrl('js/jqplot/jquery.jqplot.min.js'));
		$view->headScript()
			->appendFile($view->baseUrl('js/jqplot/excanvas.min.js'), 'text/javascript',
                        array('conditional' => 'lt IE 9'));
		$chPlugins[] = 'jqplot.barRenderer.min.js';
        $chPlugins[] = 'jqplot.categoryAxisRenderer.min.js';
        $chPlugins[] = 'jqplot.canvasTextRenderer.min.js';
        $chPlugins[] = 'jqplot.canvasAxisLabelRenderer.min.js';
        $chPlugins[] = 'jqplot.cursor.min.js';
        $chPlugins[] = 'jqplot.pointLabels.min.js';
        $chPlugins[] = 'jqplot.highlighter.min.js';
        foreach($chPlugins as $plugin)
            $view->headScript()->appendFile($view->baseUrl('js/jqplot/plugins/' . $plugin));
		$view->headScript()->appendFile($view->baseUrl('js/chart.js'));
		//$view->headScript()->appendFile($view->baseUrl('js/plugins/jquery.liveFilter.js'));
		$this->_view  = $view;

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view);
        return $view;
    }

    protected function _initAuthLayout()
    {
        $this->bootstrap('Layout');
        $layout = $this->getResource('Layout');
		$role = 'public';
        if(Zend_Auth::getInstance()->hasIdentity()):
			$layout->setLayout('login-layout');
			$this->_view->login = Zend_Auth::getInstance()->getIdentity();
		endif;

        return $layout;
    }
}
