-- MySQL dump 10.13  Distrib 5.6.27, for Linux (x86_64)
--
-- Host: localhost    Database: centralpe
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `centralpe`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `centralpe` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `centralpe`;

--
-- Table structure for table `pe_actionreport`
--

DROP TABLE IF EXISTS `pe_actionreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_actionreport` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `report_id` bigint(20) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_by` varchar(30) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id_idx` (`report_id`),
  CONSTRAINT `pe_actionreport_report_id_pe_report_id` FOREIGN KEY (`report_id`) REFERENCES `pe_report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_area`
--

DROP TABLE IF EXISTS `pe_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `code` varchar(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code` (`code`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_customer`
--

DROP TABLE IF EXISTS `pe_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(4) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code` (`code`),
  KEY `code_idx` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_imagereport`
--

DROP TABLE IF EXISTS `pe_imagereport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_imagereport` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `report_id` bigint(20) DEFAULT NULL,
  `image_title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id_idx` (`report_id`),
  CONSTRAINT `pe_imagereport_report_id_pe_report_id` FOREIGN KEY (`report_id`) REFERENCES `pe_report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_mainreport`
--

DROP TABLE IF EXISTS `pe_mainreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_mainreport` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lot` varchar(30) NOT NULL,
  `customer_code` varchar(4) DEFAULT NULL,
  `package_name` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lot` (`lot`),
  KEY `lot_idx` (`lot`),
  KEY `customer_code_idx` (`customer_code`),
  KEY `package_name_idx` (`package_name`),
  CONSTRAINT `pe_mainreport_customer_code_pe_customer_code` FOREIGN KEY (`customer_code`) REFERENCES `pe_customer` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pe_mainreport_package_name_pe_package_name` FOREIGN KEY (`package_name`) REFERENCES `pe_package` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_package`
--

DROP TABLE IF EXISTS `pe_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_package` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `name_idx` (`name`),
  KEY `group_id_idx` (`group_id`),
  CONSTRAINT `pe_package_group_id_pe_packagegroup_id` FOREIGN KEY (`group_id`) REFERENCES `pe_packagegroup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_packagegroup`
--

DROP TABLE IF EXISTS `pe_packagegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_packagegroup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `name_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_problemcategory`
--

DROP TABLE IF EXISTS `pe_problemcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_problemcategory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_reject`
--

DROP TABLE IF EXISTS `pe_reject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_reject` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `code` varchar(4) NOT NULL,
  `area_code` varchar(4) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `code_idx` (`code`),
  KEY `area_code_idx` (`area_code`),
  CONSTRAINT `pe_reject_area_code_pe_area_code` FOREIGN KEY (`area_code`) REFERENCES `pe_area` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_rejectreport`
--

DROP TABLE IF EXISTS `pe_rejectreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_rejectreport` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `report_id` bigint(20) DEFAULT NULL,
  `reject_code` varchar(4) DEFAULT NULL,
  `reject_qty` int(11) DEFAULT NULL,
  `reject_desc` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id_idx` (`report_id`),
  KEY `reject_code_idx` (`reject_code`),
  CONSTRAINT `pe_rejectreport_reject_code_pe_reject_code` FOREIGN KEY (`reject_code`) REFERENCES `pe_reject` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pe_rejectreport_report_id_pe_report_id` FOREIGN KEY (`report_id`) REFERENCES `pe_report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_report`
--

DROP TABLE IF EXISTS `pe_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lot_main` varchar(15) DEFAULT NULL,
  `area_code` varchar(4) DEFAULT NULL,
  `mc_number` varchar(5) DEFAULT NULL,
  `reject_code` varchar(4) DEFAULT NULL,
  `reject_qty` int(11) DEFAULT NULL,
  `problem_category` varchar(20) DEFAULT NULL,
  `problem_desc` varchar(255) DEFAULT NULL,
  `problem_ca` varchar(255) DEFAULT NULL,
  `problem_group` varchar(20) DEFAULT NULL,
  `problem_pic` varchar(30) DEFAULT NULL,
  `ispublic` tinyint(1) DEFAULT '1',
  `issolved` tinyint(1) DEFAULT '0',
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lot_main_idx` (`lot_main`),
  KEY `reject_code_idx` (`reject_code`),
  KEY `area_code_idx` (`area_code`),
  CONSTRAINT `pe_report_area_code_pe_area_code` FOREIGN KEY (`area_code`) REFERENCES `pe_area` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pe_report_lot_main_pe_mainreport_lot` FOREIGN KEY (`lot_main`) REFERENCES `pe_mainreport` (`lot`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pe_report_reject_code_pe_reject_code` FOREIGN KEY (`reject_code`) REFERENCES `pe_reject` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_shift`
--

DROP TABLE IF EXISTS `pe_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_shift` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(2) NOT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_user`
--

DROP TABLE IF EXISTS `pe_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `position` varchar(20) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  `shift` varchar(2) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  `role` varchar(25) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_userassignment`
--

DROP TABLE IF EXISTS `pe_userassignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_userassignment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `area_code` varchar(4) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `area_code_idx` (`area_code`),
  CONSTRAINT `pe_userassignment_area_code_pe_area_code` FOREIGN KEY (`area_code`) REFERENCES `pe_area` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pe_userassignment_user_id_pe_user_id` FOREIGN KEY (`user_id`) REFERENCES `pe_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pe_userpackage`
--

DROP TABLE IF EXISTS `pe_userpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pe_userpackage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `group_name` varchar(20) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `updated_by` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `group_name_idx` (`group_name`),
  CONSTRAINT `pe_userpackage_group_name_pe_packagegroup_name` FOREIGN KEY (`group_name`) REFERENCES `pe_packagegroup` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pe_userpackage_user_id_pe_user_id` FOREIGN KEY (`user_id`) REFERENCES `pe_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-31 22:12:29
