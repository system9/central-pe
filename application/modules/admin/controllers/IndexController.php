<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        parent::init();
        $this->_bootstrap = $this->getInvokeArg('bootstrap');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('open', 'html')
                    ->addActionContext('common', 'html')
                    ->initContext();
    }

    public function indexAction()
    {

    }

    public function backupAction()
    {

    }

    public function createBackupAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $response = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->ViewRenderer->setNoRender(true);
        $fname = Zend_Date::now()->toString('yyyyMMdd-hhmmss');
        $file = APPLICATION_PATH . '/data/tmp/' . $fname . '.sql';
        include_once('Ifsnop/Mysqldump/Mysqldump.php');
        $ini    = new Zend_Config_Ini(APPLICATION_PATH .'/configs/application.ini', APPLICATION_ENV);
        $db     = $ini->resources->db->params;
        $dump = new Ifsnop\Mysqldump\Mysqldump($db->dbname, $db->username, $db->password, $db->host);
        $dump->start($file);
        if(is_readable($file)):
            $response['backup'] = $fname . '.sql';
            $response['filesize'] = filesize($file);
        endif;
        $this->_helper->json($response);
    }

    public function downloadBackupAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->ViewRenderer->setNoRender(true);
        $f = $this->_getParam('f', false);
        $file = APPLICATION_PATH . '/data/tmp/' . $f;
        if($f && is_readable($file)):
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header('Content-Disposition: attachment;filename="' . $f .  '"');
            readfile($file);
            unlink($file);
        else:
            $this->_redirect('/');
        endif;
    }

    public function openAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();
        switch(strtolower($params['a'])):
            case 'user':
                $user = new System_Model_User();
                $this->view->user = $user->general();
                $area = new System_Model_Area();
                $this->view->area = $area->general();
                $shift = new System_Model_Shift();
                $this->view->shift = $shift->setActive()->general();
                $this->view->pkgGroup = System_Model_PackageGroup::simple();
                $this->render('user');
            break;
            case 'user-add':
                $response = array();
                $name0 = trim(strtolower($params['username']));
                $pre0 = Pe_Common::existingValue('System_Model_User', array('username' => $name0));
                if($pre0) $response['invalid'][] = $name0 . ' was duplicate';
                if(!$pre0):
                    $user = new System_Model_User();
                    $input['username'] = trim(strtolower($params['username']));
                    $input['fullname'] = trim(ucwords($params['fullname']));
                    $input['password'] = sha1($params['password']);
                    $input['isactive'] = 1;
                    $input['role']     = 'pe';
                    $user->fromArray($input);
                    $user->save();

                    $id = $user->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['user'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;
            case 'user-control':
            break;
            case 'user-control-shift':
                $user = Doctrine_Core::getTable('System_Model_User')->find($params['id']);
                $values = array();
                if($user):
                    $values['shift'] = '';
                    if($user->shift != $params['shift'])
                        $values['shift'] = trim(strtoupper($params['shift']));
                    System_Model_User::updateById($params['id'], $values);
                endif;
                $this->_helper->json($values);
            break;
            case 'user-control-assignment':
                $assign = System_Model_UserAssignment::updateAssignmentById($params['id'], $params['assign']);
                $this->_helper->json($assign);
            break;
            case 'user-control-package':
                $package = System_Model_UserPackage::updatePackageById($params['id'], $params['package']);
                $this->_helper->json($package);
            break;
            case 'reject':
                $reject = new System_Model_Reject();
                $this->view->reject = $reject->setArea($params['area'])->general();
                $this->view->area   = $params['area'];
                $this->render('reject');
            break;
            case 'reject-control':
                $response = array();
                if(array_key_exists('del', $params) && $params['del']):
                    $reject0 = Doctrine_Core::getTable('System_Model_Reject')->find($params['del']);
                    if($reject0):
                        $reject0->delete();
                        $response['del'] = 1;
                    endif;
                    $this->_helper->json($response);
                endif;
                if(array_key_exists('open', $params) && $params['open']):
                    $vars['open'] = $params['open'];
                    $vars['code'] = $params['code'];
                    $vars['name'] = $params['name'];
                    $this->view->vars = $vars;
                    $this->render('reject-control');
                endif;
                if(array_key_exists('update', $params) && $params['update']):
                    $pre0 = Pe_Common::existingValue('System_Model_Reject',
                                array('name' => trim(ucwords($params['name']))),
                                $params['update']);
                    $pre1 = Pe_Common::existingValue('System_Model_Reject',
                                array('code' => trim(ucwords($params['code']))),
                                $params['update']);
                    if($pre0) $response['invalid'][] = $params['name'] . ' has been used';
                    if($pre1) $response['invalid'][] = $params['code'] . ' has been used';
                    if(!$pre0 && !$pre1):
                        $input['name'] = trim(ucwords($params['name']));
                        $input['code'] = trim(strtoupper($params['code']));
                        System_Model_Reject::updateById($params['update'], $input);
                        $response['update'] = array('code' => $input['code'], 'name' => $input['name']);
                    endif;
                    $this->_helper->json($response);
                endif;
            break;
            case 'reject-add':
                $name0 = trim(ucwords($params['name']));
                $code0 = trim(strtoupper($params['code']));
                $area0 = $params['area_code'];
                $reject0 = 'System_Model_Reject';
                $pre0 = Pe_Common::existingValue($reject0, array('name' => $name0));
                $pre1 = Pe_Common::existingValue($reject0, array('code' => $code0));
                if($pre0) $response['invalid'][] = $name0 . ' was duplicate';
                if($pre1) $response['invalid'][] = $code0 . ' was duplicate';

                if(!$pre0 && !$pre1):
                    $input['name'] = $name0;
                    $input['code'] = $code0;
                    $input['area_code'] = $area0;
                    $reject = new System_Model_Reject();
                    $reject->fromArray($input);
                    $reject->save();

                    $id = $reject->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['reject'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;
            case 'customer':
                $customer = new System_Model_Customer();
                $this->view->customer = $customer->general();
                $this->render('customer');
            break;
            case 'customer-add':
                $name0 = trim(ucwords($params['name']));
                $code0 = trim(strtoupper($params['code']));
                $cust0 = 'System_Model_Customer';
                $pre0 = Pe_Common::existingValue($cust0, array('name' => $name0));
                $pre1 = Pe_Common::existingValue($cust0, array('code' => $code0));
                if($pre0) $response['invalid'][] = $name0 . ' was duplicate';
                if($pre1) $response['invalid'][] = $code0 . ' was duplicate';
                if(!$pre0 && !$pre1):
                    $customer = new System_Model_Customer();
                    $input['name'] = trim(ucwords($params['name']));
                    $input['code'] = trim(strtoupper($params['code']));
                    $customer->fromArray($input);
                    $customer->save();

                    $id = $customer->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['customer'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;
            case 'customer-control':
                $response = array();
                if(array_key_exists('del', $params) && $params['del']):
                    $cust0 = Doctrine_Core::getTable('System_Model_Customer')->find($params['del']);
                    if($cust0):
                        $cust0->delete();
                        $response['del'] = 1;
                    endif;
                    $this->_helper->json($response);
                endif;
                if(array_key_exists('open', $params) && $params['open']):
                    $vars['open'] = $params['open'];
                    $vars['code'] = $params['code'];
                    $vars['name'] = $params['name'];
                    $this->view->vars = $vars;
                    $this->render('customer-control');
                endif;
                if(array_key_exists('update', $params) && $params['update']):
                    $pre0 = Pe_Common::existingValue('System_Model_Customer',
                                array('name' => trim(ucwords($params['name']))),
                                $params['update']);
                    $pre1 = Pe_Common::existingValue('System_Model_Customer',
                                array('code' => trim(ucwords($params['code']))),
                                $params['update']);
                    if($pre0) $response['invalid'][] = $params['name'] . ' has been used';
                    if($pre1) $response['invalid'][] = $params['code'] . ' has been used';
                    if(!$pre0 && !$pre1):
                        $input['name'] = trim(ucwords($params['name']));
                        $input['code'] = trim(strtoupper($params['code']));
                        System_Model_Customer::updateById($params['update'], $input);
                        $response['update'] = array('code' => $input['code'], 'name' => $input['name']);
                    endif;
                    $this->_helper->json($response);
                endif;
            break;
            case 'area':
                $area = new System_Model_Area();
                $this->view->area = $area->general();
                $this->render('area');
            break;
            case 'area-add':
                $pre0 = Pe_Common::existingValue('System_Model_Area',
                            array('name' => trim(ucwords($params['name']))));
                $pre1 = Pe_Common::existingValue('System_Model_Area',
                            array('code' => trim(ucwords($params['code']))));
                if($pre0) $response['invalid'][] = $params['name'] . ' was duplicate';
                if($pre1) $response['invalid'][] = $params['code'] . ' was duplicate';
                if($pre0 == 0 && $pre1 == 0):
                    $area = new System_Model_Area();
                    $input['name'] = trim(ucwords($params['name']));
                    $input['code'] = trim(strtoupper($params['code']));
                    $area->fromArray($input);
                    $area->save();

                    $id = $area->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['area'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;
            case 'area-control':
                $response = array();
                if(array_key_exists('del', $params) && $params['del']):
                    $area0 = Doctrine_Core::getTable('System_Model_Area')->find($params['del']);
                    if($area0):
                        $area0->delete();
                        $response['del'] = 1;
                    endif;
                    $this->_helper->json($response);
                endif;
                if(array_key_exists('open', $params) && $params['open']):
                    $vars['open'] = $params['open'];
                    $vars['code'] = $params['code'];
                    $vars['name'] = $params['name'];
                    $this->view->vars = $vars;
                    $this->render('area-control');
                endif;
                if(array_key_exists('update', $params) && $params['update']):
                    $pre0 = Pe_Common::existingValue('System_Model_Area',
                                array('name' => trim(ucwords($params['name']))),
                                $params['update']);
                    $pre1 = Pe_Common::existingValue('System_Model_Area',
                                array('code' => trim(ucwords($params['code']))),
                                $params['update']);
                    if($pre0) $response['invalid'][] = $params['name'] . ' has been used';
                    if($pre1) $response['invalid'][] = $params['code'] . ' has been used';
                    if(!$pre0 && !$pre1):
                        $input['name'] = trim(ucwords($params['name']));
                        $input['code'] = trim(strtoupper($params['code']));
                        System_Model_Area::updateById($params['update'], $input);
                        $response['update'] = array('code' => $input['code'], 'name' => $input['name']);
                    endif;
                    $this->_helper->json($response);
                endif;
            break;
            case 'shift':
                $shift = new System_Model_Shift();
                $this->view->shift = $shift->general();
                $this->render('shift');
            break;
            case 'shift-add':
                $shift0 = trim(strtoupper($params['shift']));
                $pre0 = Pe_Common::existingValue('System_Model_Shift', array('name' => $shift0));
                if($pre0) $response['invalid'][] = $shift0 . ' was duplicate';
                if(!$pre0):
                    $shift = new System_Model_Shift();
                    $input = array('name' => trim(strtoupper($params['shift'])), 'isactive' => 1);
                    $shift->fromArray($input);
                    $shift->save();

                    $id = $shift->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['shift'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;
            case 'shift-control':
                $response = array();
                if(array_key_exists('del', $params) && $params['del']):
                    $shift0 = Doctrine_Core::getTable('System_Model_Shift')->find($params['del']);
                    if($shift0):
                        $shift0->delete();
                        $response['del'] = 1;
                    endif;
                    $this->_helper->json($response);
                endif;
                if(array_key_exists('upd', $params) && $params['upd']):
                    $upd = System_Model_Shift::switchStatusById($params['upd']);
                    $this->_helper->json($upd);
                endif;
            break;
            case 'package-group':
                $pkgGroup = new System_Model_PackageGroup();
                $this->view->packagegroup = $pkgGroup->general();
                $this->render('package-group');
            break;
            case 'package-group-add':
                $name0 = trim(strtoupper($params['name']));
                $pre0 = Pe_Common::existingValue('System_Model_PackageGroup', array('name' => $name0));
                if($pre0) $response['invalid'][] = $name0 . ' was duplicate';
                if(!$pre0):
                    $pkgGroup = new System_Model_PackageGroup();
                    $input = array('name' => $name0);
                    $pkgGroup->fromArray($input);
                    $pkgGroup->save();

                    $id = $pkgGroup->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['packagegroup'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;
            case 'package-group-control':
                $response = array();
                if(array_key_exists('del', $params) && $params['del']):
                    $pg0 = Doctrine_Core::getTable('System_Model_PackageGroup')->find($params['del']);
                    if($pg0):
                        $pg0->delete();
                        $response['del'] = 1;
                    endif;
                    $this->_helper->json($response);
                endif;
                if(array_key_exists('upd', $params) && $params['upd']):
                    $nm = strtoupper(trim(ucwords($params['nm'])));
                    $pre0 = Pe_Common::existingValue('System_Model_PackageGroup',
                        array('name' => $nm), $params['upd']);

                    if($pre0) $response['invalid'][] = $nm . ' has been used';
                    if(!$pre0):
                        $input['name'] = $nm;
                        System_Model_PackageGroup::updateById($params['upd'], $input);
                        $response['update'] = $nm;
                    endif;
                    $this->_helper->json($response);
                endif;
            break;
            case 'package-add':
                $name0 = trim(strtoupper($params['p']));
                $pre0 = Pe_Common::existingValue('System_Model_Package', array('name' => $name0));
                if($pre0) $response['invalid'][] = $name0 . ' was duplicate';
                if(!$pre0):
                    $pkg = new System_Model_Package();
                    $input['name'] = trim(strtoupper($params['p']));
                    $input['group_id'] = $params['g'];
                    $pkg->fromArray($input);
                    $pkg->save();

                    $id = $pkg->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['package'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;
            case 'package-control':
                $response = array();
                if(array_key_exists('del', $params) && $params['del']):
                    $pg0 = Doctrine_Core::getTable('System_Model_Package')->find($params['del']);
                    if($pg0):
                        $pg0->delete();
                        $response['del'] = 1;
                    endif;
                    $this->_helper->json($response);
                endif;
                if(array_key_exists('upd', $params) && $params['upd']):
                    $nm = strtoupper(trim(ucwords($params['nm'])));
                    $pre0 = Pe_Common::existingValue('System_Model_Package',
                        array('name' => $nm), $params['upd']);

                    if($pre0) $response['invalid'][] = $nm . ' has been used';
                    if(!$pre0):
                        $input['name'] = $nm;
                        System_Model_Package::updateById($params['upd'], $input);
                        $response['update'] = $nm;
                    endif;
                    $this->_helper->json($response);
                endif;
            break;
            case 'report-type':
                $rTypeDef = new System_Model_ProblemCategory();
                $this->view->rTypeDef = $rTypeDef->general();
                $this->render('report-type');
            break;
            case 'report-type-control':
                $response = array();
                if(array_key_exists('del', $params) && $params['del']):
                    $cat0 = Doctrine_Core::getTable('System_Model_ProblemCategory')->find($params['del']);
                    if($cat0):
                        $cat0->delete();
                        $response['del'] = 1;
                    endif;
                    $this->_helper->json($response);
                endif;
            break;
            case 'report-type-add':
                $cat0 = trim(ucwords($params['name']));
                $pre0 = Pe_Common::existingValue('System_Model_ProblemCategory', array('name' => $cat0));
                if($pre0) $response['invalid'][] = $cat0 . ' was duplicate';
                if(!$pre0):
                    $rTypeDef = new System_Model_ProblemCategory();
                    $input = array('name' => trim(ucwords($params['name'])));
                    $rTypeDef->fromArray($input);
                    $rTypeDef->save();

                    $id = $rTypeDef->identifier();
                    if($id && $id['id']):
                        $input['ref-id'] = $id['id'];
                        $response['reporttype'] = $input;
                    endif;
                endif;
                $this->_helper->json($response);
            break;

            default:
            break;
        endswitch;
    }

    public function commonAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();
        $response = array();
        switch(strtolower($params['a'])):
            case 'pre-validation':
                $model = 'System_Model_' . ucfirst($params['m']);
                $values[$params['f']] = $params['v'];
                if(array_key_exists('f2', $params) && array_key_exists('v2', $params)):
                    $values[trim($params['f2'])] = trim($params['v2']);
                endif;
                $this->_helper->json(array('value' => Pe_Common::existingValue($model, $values)));
            break;
            default:
            break;
        endswitch;
    }


}
