<?php

class Dashboard_ApiController extends Zend_Controller_Action
{
    protected $_user;
    public function init()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $user = new Pe_User();
        $this->_user = $user->properties();
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        // action body
    }

    public function sourceChartAction()
    {
        $search     = $this->_getParam('search', false);
        $byGroup    = $this->_getParam('bygroup', false);
        $byPackage  = $this->_getParam('bypackage', false);
        $byMachine  = $this->_getParam('bymachine', false);
        $byRejectGroup  = $this->_getParam('byrejectgroup', false);
        $byRejectCode   = $this->_getParam('byrejectcode', false);
        $userBased      = $this->_getParam('user', false);
        $userActive     = $userBased ? $this->_user['username'] : false;
        $date0 = $this->_getParam('ww0', Zend_Date::now()->subDay(15)->toString('yyyy-MM-dd'));
        $date1 = $this->_getParam('ww1', Zend_Date::now()->addDay(1)->toString('yyyy-MM-dd'));
        $general = new System_Model_MainReport();
        $g0 = $general->setPackage($byPackage)
                    ->setArea($byRejectGroup)->setRejectCode($byRejectCode)
                    ->setReportBy($userActive)
                    ->setMcNumber($byMachine)
                    ->setStartDate($date0)->setBeforeDate($date1)
                    ->setPackageGroup($byGroup)->searchBylot($search)->general();
        $response    = array();
        if(array_key_exists('package', $g0['source'])):
            $srcPackage  = Pe_Common::chartSourceGeneralConverter($g0['source']['package']);
            $srcCustomer = Pe_Common::chartSourceGeneralConverter($g0['source']['customer']);
            $srcProblem  = Pe_Common::chartSourceGeneralConverter($g0['source']['problem']);
            $srcMc       = Pe_Common::chartSourceGeneralConverter($g0['source']['mc']);
            $response['package'] = array('source' => $srcPackage, 'title' => 'By Package');
            $response['problem'] = array('source' => $srcProblem, 'title' => 'By Problem');
            $response['customer'] = array('source' => $srcCustomer, 'title' => 'By Customer');
            $response['mc'] = array('source' => $srcMc, 'title' => 'By Machine');
        endif;

        $this->_helper->json($response);
    }

    public function customerListAction()
    {
        $cust = new System_Model_Customer();
        $cust = $cust->general();
        $response = array();
        if(count($cust)) $response['customer'] = $cust;
        $this->_helper->json($response);
    }

    public function packagesAssignmentAction()
    {
        $user = new Pe_User();
        $user = $user->properties();
        $response = array();
        if(count($user['group-packages'])) $response['groups'] = $user['group-packages'];
        if(count($user['package-list'])) $response['packages'] = $user['package-list'];
        $this->_helper->json($response);
    }


}
