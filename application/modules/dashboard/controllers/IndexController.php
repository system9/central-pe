<?php

class Dashboard_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        parent::init();
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('open', 'html')
                    ->addActionContext('support', 'html')
                    ->initContext();
    }

    public function indexAction()
    {
        $pGroup = new System_Model_PackageGroup();
        $this->view->pGroup = $pGroup->general();
        $aReject = new System_Model_Area();
        $this->view->areaReject = $aReject->general();
        $report     = new System_Model_MainReport();
        $date0 = Zend_Date::now()->subDay(15)->toString('yyyy-MM-dd');
        $report0    = $report->setStartDate($date0)->general();
        if(count($report0['source'])):
            $this->view->bymachine = Pe_Common::chartSourceGeneralConverter($report0['source']['mc']);
        endif;
        $this->view->report = $report0['list'];
        $this->view->wwList = Pe_Common::wwList();
    }

    public function downloadAction()
    {
        $report = $this->_getParam('report', 'general');
        $this->_helper->layout->disableLayout();
        $this->_helper->ViewRenderer->setNoRender(true);

        switch(trim(strtolower($report))):
            case 'general':
                $general    = new System_Model_MainReport();
                $excel      = new Pe_Excel();
                $search     = $this->_getParam('search', false);
                $byGroup    = $this->_getParam('bygroup', false);
                $byPackage  = $this->_getParam('bypackage', false);
                $dtRef = new Zend_Date();
                $date0 = $this->_getParam('ww0', Zend_Date::now()->subDay(15)->toString('yyyy-MM-dd'));
                $date1 = $this->_getParam('ww1', Zend_Date::now()->addDay(1)->toString('yyyy-MM-dd'));
                $g0 = $general->setPackage($byPackage)
                            ->setStartDate($date0)->setBeforeDate($date)
                            ->setPackageGroup($byGroup)->searchBylot($search)->general();
                $excel->general($g0['list']);
            break;
            default:
            break;
        endswitch;
    }

    public function openAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();
        switch(strtolower($params['a'])):
            case 'report-detail':
                $report = new System_Model_MainReport();
                $report = $report->setReportId($params['id'])->general();
                if(count($report['list'])):
                    $main = $report['list'][0];
                    $this->view->report = $main;
                    if(count($main['report'])):
                        $detail = $main['report'][0];
                        $this->view->images = System_Model_ImageReport::picturesByReportId($detail['id']);
                    endif;
                endif;
                $this->render('report-detail');
            break;
            case 'search-lots':
                $report = new System_Model_MainReport();
                $search = false;
                $urlAppx = '?';
                if(array_key_exists('search', $params)):
                    $search = $params['search'];
                    $urlAppx .= '&search=' . $search;
                endif;
                $byGroup = false;
                if(array_key_exists('bygroup', $params)):
                     $byGroup = $params['bygroup'];
                     $urlAppx .= '&bygroup=' . $byGroup;
                endif;
                $byPackage = false;
                if(array_key_exists('bypackage', $params)):
                    $byPackage = $params['bypackage'];
                    $urlAppx   .= '&bypackage=' . $byPackage;
                endif;
                $byRejectGroup = false;
                if(array_key_exists('byrejectgroup', $params) && $params['byrejectgroup']):
                    $byRejectGroup = $params['byrejectgroup'];
                    $urlAppx   .= '&byrejectgroup=' . $byRejectGroup;
                endif;
                $byRejectCode = false;
                if(array_key_exists('byrejectcode', $params) && $params['byrejectcode']):
                    $byRejectCode = $params['byrejectcode'];
                    $urlAppx   .= '&byrejectcode=' . $byRejectCode;
                endif;
                $byMachine = false;
                if(array_key_exists('bymachine', $params) && $params['bymachine']):
                    $byMachine = $params['bymachine'];
                    $urlAppx   .= '&bymachine=' . $byMachine;
                endif;
                $dtRef = new Zend_Date();
                $date0 = $dtRef->subDay(15)->toString('yyyy-MM-dd');
                if(array_key_exists('ww0', $params) && $params['ww0']) $date0 = $params['ww0'];
                $urlAppx   .= '&ww0=' . $date0;

                $date1 = false;
                if(array_key_exists('ww1', $params) && $params['ww1']):
                    $date1 = $params['ww1'];
                    $urlAppx   .= '&ww1=' . $date1;
                endif;

                $this->view->urlappx = $urlAppx;
                $report = $report->setPackage($byPackage)
                            ->setArea($byRejectGroup)->setRejectCode($byRejectCode)
                            ->setMcNumber($byMachine)
                            ->setStartDate($date0)->setBeforeDate($date1)
                            ->setPackageGroup($byGroup)->searchBylot($search)->general();
                $report = $report['list'];

                if(count($report) && count($report[0]['report'])):
                    if(count($report) == 1):
                        if(count($report[0]['report']) == 1):
                            $this->view->report = $report[0];
                            $reportId = $report[0]['report'][0]['id'];
                            $this->view->images = System_Model_ImageReport::picturesByReportId($reportId);
                            $this->render('report-detail');
                        else:
                            $this->view->report = $report[0];
                            $this->render('lot-history');
                        endif;
                    else:
                        $this->view->report = $report;
                        $this->render('search-by-lot');
                    endif;

                else:
                    $this->render('search-by-lot');
                endif;
            break;
            default:
            break;
        endswitch;
    }


}
