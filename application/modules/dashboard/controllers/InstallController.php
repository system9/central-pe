<?php

class Dashboard_InstallController extends Zend_Controller_Action
{
    public function init()
    {
        parent::init();
        $this->_bootstrap = $this->getInvokeArg('bootstrap');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('xhr', 'html')
                    ->initContext();
    }

    public function indexAction()
    {
        $users = Doctrine_Core::getTable('System_Model_User')->findAll();

        if(!count($users)):
            $form = new Dashboard_Form_InitUser();
            $form->setAction('/install/xhr/do/init-user/format/html');
            $this->view->form = $form;
        else:
            $this->_redirect('/');
        endif;

        $this->view->headScript()->appendFile($this->view->baseUrl('js/validate/jquery.metadata.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/validate/jquery.validate.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/form/jquery.form.js'));
    }

    public function xhrAction()
    {
        if(!$this->_request->isXmlHttpRequest()):
            $this->_redirect('/install');
        endif;

        $params = $this->_getAllParams();
        switch($params['do']):
            case 'init-user':
                $this->_helper->ViewRenderer->setNoRender(true);
                $params['username'] = trim(strtolower($params['username']));
                $params['fullname'] = trim($params['fullname']);
                $params['password'] = sha1($params['password']);
                $params['department'] = 'NA';
                $params['isactive'] = '1';
                $params['role'] = 'system-admin';
                $user = new System_Model_User();
                $user->fromArray($params);
                $user->save();

                $users = Doctrine_Core::getTable('System_Model_User')->findAll();
                if(count($users)):
                    echo Zend_Json::encode(array('confirm' => 'Succesfully added'));
                else:
                    echo Zend_Json::encode(array('warning' => 'Failed to install'));
                endif;

            break;
            default:
            break;
        endswitch;
    }

}
