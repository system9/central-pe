<?php

class Dashboard_LoginController extends Zend_Controller_Action
{
    protected $_auth = null;
    protected $_role = 'PUBLIC';
    
    public function init()
    {
        parent::init();
        $this->_bootstrap = $this->getInvokeArg('bootstrap');
        $this->_auth = Zend_Auth::getInstance();
        if($this->_auth->hasIdentity()):
            $this->_role = strtoupper($this->_auth->getIdentity()->role);
        endif;
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('xhr', 'html')
                    ->initContext();
    }

    public function indexAction()
    {
        $form = new Dashboard_Form_Login();
        $dbAdapter = $this->_bootstrap->getPluginResource('db')->getDbAdapter();

        if(!$this->_auth->hasIdentity()):
            if($this->_request->isPost() && $form->isValid($_POST)):
                $values = $form->getValues();
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
                $authAdapter->setTableName('pe_user')
                        ->setIdentityColumn('username')
                        ->setCredentialColumn('password')
                        ->setIdentity($values['username'])
                        ->setCredential(sha1($values['password']));
                        //->setCredentialTreatment('sha1(?)');
                
                $result = $this->_auth->authenticate($authAdapter);
                $authStatus = $result->isValid();

                if($authStatus == true):
                    $storage = $this->_auth->getStorage(); // new Zend_Auth_Storage_Session();
                    //$storage = new Zend_Auth_Storage_Session('shimano');
                    $storage->write($authAdapter->getResultRowObject(null, 'password'));

                    $loggedUser = $this->_auth->getIdentity()->username;
                    $user = Doctrine_Core::getTable('System_Model_User')->findOneBy('username', $loggedUser);
                    $this->view->user = $user;
                    $this->_redirect('/');
                else:
                    if ($this->getInvokeArg('displayExceptions') == true):
                        $this->view->messages = $result->getMessages();
                    endif;
                    $this->view->form = $form;
                    $this->view->notification  = 'The username and/or password  was incorrect.';
                endif;
            else:
                $this->view->form = $form;
            endif;
        else:
            $loggedUser = $this->_auth->getIdentity()->username;
            $user = Doctrine_Core::getTable('System_Model_User')->findOneBy('username', $loggedUser);
            $this->view->user = $user;
            $this->view->isAdmin = $this->_role == 'SUPERUSER' ? $this->_role : false;
            $this->_redirect('/');
        endif;

        $this->view->headScript()->appendFile($this->view->baseUrl('js/ui/jquery.ui.core.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/ui/jquery.effects.core.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/ui/jquery.effects.fold.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/validate/jquery.metadata.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/validate/jquery.validate.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/form/jquery.form.js'));
    }
    
    public function outAction()
    {
        $this->_helper->ViewRenderer->setNoRender(true);
        if($this->_auth->hasIdentity()):
            $this->_auth->clearIdentity();
        endif;
        $this->_redirect('/');
    }
}

