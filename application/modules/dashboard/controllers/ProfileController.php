<?php

class Dashboard_ProfileController extends Zend_Controller_Action
{
    protected $_user;
    public function init()
    {
        parent::init();
        $user = new Pe_User();
        $this->_user = $user->properties();
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('activity', 'html')
                    ->addActionContext('support', 'html')
                    ->initContext();
    }

    public function indexAction()
    {
        $this->view->area       = System_Model_Area::codeToName();
        $this->view->properties = $this->_user;
    }

    public function activityAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();
        $response = array();
        switch(strtolower($params['a'])):
            case 'change-password':
                $uid = $this->_user['id'];
                $update['password'] = sha1($params['password']);
                System_Model_User::updateById($uid, $update);
            break;
            default:
            break;
        endswitch;
    }


}
