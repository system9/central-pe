<?php

class Dashboard_ShiftlyController extends Zend_Controller_Action
{
    protected $_login;
    protected $_user;
    public function init()
    {
        parent::init();
        $user = new Pe_User();
        $this->_user = $user->properties();
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('open', 'html')
                    ->addActionContext('support', 'html')
                    ->initContext();
    }

    public function indexAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $this->view->wwList = Pe_Common::wwList();
        $report = new System_Model_MainReport();
        $date0 = Zend_Date::now()->subDay(2)->toString('yyyy-MM-dd');
        $report = $report->setStartDate($date0)
                         ->setReportBy($this->_user['username'])->general();
        $this->view->report = $report['list'];
    }

    public function uploadImageAction()
    {
        $this->_helper->layout->setLayout('blank-layout');
        $imgId = $this->_getParam('img', false);
        $lot   = $this->_getParam('lot', false);
        if($lot):
            if($imgId):
                $this->view->img = System_Model_ImageReport::onePictureById($imgId);
            endif;

            $report = System_Model_Report::getLotInfoById($lot);
            if($report):
                $folderReject = WWW_ROOT . DIRECTORY_SEPARATOR . 'pictures'
                                . DIRECTORY_SEPARATOR . strtoupper($report->reject);
                if(!is_readable($folderReject)) mkdir($folderReject);
                $folderRejectPackage = $folderReject
                    . DIRECTORY_SEPARATOR . strtoupper($report->package_name);
                if(!is_readable($folderRejectPackage)) mkdir($folderRejectPackage);

                $form = new Dashboard_Form_Upload();
                $form->addElement('hidden', 'id', array(
                    'required' => true, 'order' => 0,
                    'readonly' => 'readonly', 'value' => $report->id));
                $form->addElement('hidden', 'lot', array('value' => $lot,
                        'order' => '1', 'readonly' => 'readonly'));
                $elementFile = new Zend_Form_Element_File('image');
                $elementFile->setAttribs(array('class' => 'input-large'))
                     ->setDestination($folderReject)
                     ->addValidator('Count', false, 1)
                     ->setOrder(2)
                     ->addValidator('Extension', false, 'jpg, jpeg, png')
                     ->setRequired(true)
                     ->setLabel('Upload Image');
                $form->addElement($elementFile);

                if($this->_request->isPost()):
                    $postData   = $this->_request->getPost();
                    $isValid    = $form->isValid($postData);
                    if (!$isValid) return;

                    $image  = $form->getElement('image');
                    $input['report_id']   = $lot;
                    $input['image_title'] = $lot . ' - ' . strtoupper($report->package_name)
                                            . ' - ' . strtoupper($report->reject);
                    $imgReport = new System_Model_ImageReport();
                    $imgReport->fromArray($input);
                    $imgReport->save();
                    $imgId = $imgReport->identifier();
                    $newFile = $lot;
                    if($imgId && $imgId['id']) $newFile .= '-' . $imgId['id'];
                    $newFile .= '.jpg';

                    $image->addFilter('Rename',
                        array('target' => $folderRejectPackage . DIRECTORY_SEPARATOR . $newFile,
                                'overwrite' => true), null, $image);
                    if ($image->receive()):
                        $this->_redirect('/shiftly/upload-image/lot/' . $lot . '/img/' . $imgId['id']);
                    else:
                        $this->view->notice = 'Image file to upload';
                    endif;
                else:
                    $this->view->form = $form;
                endif;
            else:
                $this->view->notification = 'Report not exist';
            endif;
        else:
            $this->view->notification = 'No Lot has been selected';
        endif;


    }

    public function openAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();
        $response = array();
        switch(strtolower($params['a'])):
            case 'shiftly':
                $cust = new System_Model_Customer();
                $attributes['customer']     = $cust->general();
                $attributes['user-area']    = $this->_user['assignment'];
                $attributes['reject-list']  = $this->_user['reject-list'];
                $attributes['package-list'] = $this->_user['package-list'];
                $rTypeDef = new System_Model_ProblemCategory();
                $attributes['problem-type'] = $rTypeDef->general();
                $shift = new System_Model_Shift();
                $attributes['shift']        = $shift->setActive()->general();
                $this->view->attributes     = $attributes;
                $this->render('shiftly');
            break;
            case 'shiftly-get-lot':
                $main = new System_Model_MainReport();
                $lot  = $main->setLot($params['lot'])->general();
                if(count($lot['list'])) $response['lot'] = $lot['list'][0];
                $this->_helper->json($response);
            break;
            case 'shiftly-add':
                $input0['lot']   = trim(strtoupper($params['lot']));
                $input0['idref'] = false;
                if(array_key_exists('package_name', $params)
                    && array_key_exists('customer_code', $params)):
                    $input0['package_name']  = $params['package_name'];
                    $input0['customer_code'] = $params['customer_code'];
                    $report0 = new System_Model_MainReport();
                    $report0->fromArray($params);
                    $report0->save();
                    $idReport0 = $report0->identifier();
                    if($idReport0 && $idReport0['id']):
                        $input0['idref']  = $idReport0['id'];
                        $response['main'] = $input0;
                        $this->view->main = $input0;
                    endif;
                else:
                    $m0 = Doctrine_Core::getTable('System_Model_MainReport')
                            ->findOneBy('lot', $input0['lot']);
                    $input0['idref'] = $m0->id;
                    $input0['package_name']  = $m0->package_name;
                    $input0['customer_code'] = $m0->customer_code;
                    $this->view->main = $input0;
                endif;
                if($input0['idref']):
                    $input1['lot_main']     = $input0['lot'];
                    $input1['area_code']    = $params['area_code'];
                    $input1['mc_number']    = $params['mc_number'];
                    $input1['reject_code']  = $params['reject_code'];
                    $input1['reject_qty']   = $params['reject_qty'];
                    $input1['problem_category'] = $params['problem_category'];
                    $input1['problem_desc'] = trim($params['problem_desc']);
                    $input1['problem_ca']   = trim($params['problem_ca']);
                    $input1['problem_pic']  = $params['problem_pic'];
                    if(array_key_exists('shift', $params))
                        $input1['problem_group'] = implode(', ', $params['shift']);
                    $report1 = new System_Model_Report();
                    $report1->fromArray($input1);
                    $report1->save();
                    $idReport1 = $report1->identifier();
                    if($idReport1 && $idReport1['id']):
                        $input1['idref']    = $idReport1['id'];
                        $rConvertion = Pe_Common::rejectConvertionByCode();
                        $input1['reject-name'] = $input1['reject_code'];
                        if(array_key_exists($input1['reject_code'], $rConvertion))
                            $input1['reject-name'] = $rConvertion[$input1['reject_code']]['name'];
                        $this->view->detail = $input1;
                        $this->view->images = System_Model_ImageReport::picturesByReportId($input1['idref']);
                    endif;
                endif;
                $this->render('shiftly-add');
            break;
            case 'shiftly-today':
                $report = new System_Model_MainReport();
                $dtRef = new Zend_Date();
                $date0 = $this->_getParam('ww0', Zend_Date::now()->subDay(15)->toString('yyyy-MM-dd'));
                $date1 = $this->_getParam('ww1', Zend_Date::now()->addDay(1)->toString('yyyy-MM-dd'));
                $report = $report->setReportBy($this->_user['username'])
                        ->setStartDate($date0)->setBeforeDate($date1)
                        ->general();
                $this->view->report = $report['list'];
                $this->render('shiftly-today');
            break;
            case 'shiftly-my':
            break;
            case 'shiftly-all':
            break;
            case 'report-detail':
                $report = new System_Model_MainReport();
                $report = $report->setHour(24)->setReportId($params['id'])->general();
                $report = $report['list'];
                if(count($report)):
                    $main = $report[0];
                    $this->view->report = $main;
                    if(count($main['report'])):
                        $detail = $main['report'][0];
                        $this->view->images = System_Model_ImageReport::picturesByReportId($detail['id']);
                    endif;
                endif;
                $this->render('report-detail');
            break;
            case 'report-control':
                $response = array();
                if(array_key_exists('upd', $params) && $params['upd']):
                    if(array_key_exists('f2', $params) && array_key_exists('v2', $params))
                        $upd[$params['f2']] = trim($params['v2']);
                    if(array_key_exists('f', $params) && array_key_exists('v', $params)):
                        $upd[$params['f']] = trim($params['v']);
                        System_Model_Report::updateById($params['upd'], $upd);
                        $this->_helper->json($upd);
                    endif;
                endif;
                if(array_key_exists('del', $params) && $params['del']):
                    $rpt = Doctrine_Core::getTable('System_Model_Report')->find($params['del']);
                    if($rpt):
                        $rpt->delete();
                        $response['del'] = $params['del'];
                        $img = System_Model_ImageReport::reportDeleted($params['del']);
                        if(count($img)):
                            $response['image'] = $img;
                            $response['delete-image'] = count($img);
                        endif;
                    endif;
                    $this->_helper->json($response);
                endif;
                if(array_key_exists('main', $params) && $params['main']):
                    $upd = array();
                    if(array_key_exists('cust', $params)
                        && $params['cust']) $upd['customer_code'] = $params['cust'];
                    if(array_key_exists('pkg', $params)
                        && $params['pkg']) $upd['package_name'] = $params['pkg'];
                    System_Model_MainReport::updateByLot($params['main'], $upd);
                    $this->_helper->json($upd);
                endif;
            break;

            case 'image-report-control':
                if(array_key_exists('del', $params) && $params['del']):
                    $img = Doctrine_Core::getTable('System_Model_ImageReport')->find($params['del']);
                    if(array_key_exists('f', $params) && $params['f'])
                        unlink(WWW_ROOT . DIRECTORY_SEPARATOR . $params['f']);
                    if($img):
                        $img->delete();
                        $this->_helper->json(array('del' => 'Image has been deleted'));
                    endif;
                endif;
            break;
            case 'get-reject-by-area':
                $reject = new System_Model_Reject();
                $this->_helper->json($reject->setArea($params['area'])->general());
            break;
            case 'explore-ww':
                $report = new System_Model_MainReport();
                $dtRef = new Zend_Date();
                $date0 = $this->_getParam('ww0', $dtRef->subDay(15)->toString('yyyy-MM-dd'));
                $date1 = $this->_getParam('ww1', $dtRef->toString('yyyy-MM-dd'));
                $this->view->urlappx = '?user=1&ww0=' . $date0 . '&ww1=' . $date1;
                $report = $report->setReportBy($this->_user['username'])
                        ->setStartDate($date0)->setBeforeDate($date1)
                        ->general();
                $this->view->report = $report['list'];
                $this->render('shiftly-today');
            break;
            default:
            break;
        endswitch;
    }

    public function supportAction()
    {
        if(!$this->_request->isXmlHttpRequest()) $this->_redirect('/');
        $this->_helper->ViewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $params     = $this->_getAllParams();
        $response   = array();
        switch(strtolower($params['a'])):
            case 'reject-list':
                if(array_key_exists('reject-list', $this->_user))
                    $response['reject-list'] = $this->_user['reject-list'];
                $this->_helper->json($response);
            break;
            default:
            break;
        endswitch;
    }
}
