<?php

class Dashboard_Form_InitUser extends Zend_Form
{

    public function init()
    {
        $this->setAttribs(array('class' => 'form-vertical', 'id'=>'init-user'));

        $this->addElement('text', 'username', array(
            'label' => 'Username',
            'value' => 'administrator',
            'required' => true,
            'attribs' => array('class' => 'required unique',
                               'placeholder' => 'Username')));
        $this->addElement('text', 'fullname', array(
            'label' => 'Fullname',
            'value' => 'Administrator',
            'required' => true,
            'attribs' => array('class' => 'required')));

        $this->addElement('password', 'password', array(
            'label' => 'Password',
            'required' => true,
            'attribs' => array('class' => 'required', 'minlength' => 6)));

        $this->addElement('password', 'confirm', array(
            'label' => 'Confirm Password',
            'attribs' => array('class' => 'required', 'minlength' => 6, 'equalTo' => '#password')));

        $this->addElement('submit', 'submit', array(
            'label' => 'Create User',
            'attribs' => array('class' => 'btn btn-large btn-inverse'),
            'ignore' => true));
    }


}

