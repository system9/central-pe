<?php

class Dashboard_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setAttribs(array('id' => 'form-login', 'class' => 'form-horizontal'));
        $this->addElement('text', 'username', array(
            'label' => 'Username',
            'required' => true,
            'validators' => array(),
            'filters' => array(),
            'attribs' => array('class' => 'required input-medium',
                                'placeholder' => 'Username')
            ));
        $this->addElement('password', 'password', array(
            'label' => 'Password',
            'required' => true,
            'attribs' => array('class' => 'required input-medium',
                                'placeholder' => 'Password')
            ));
        $this->addElement('submit', 'submit', array(
            'label' => 'Log In',
            'ignore' => true,
            'class' => 'btn btn-primary btn-block'
            ));
    }


}
