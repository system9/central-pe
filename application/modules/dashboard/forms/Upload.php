<?php

class Dashboard_Form_Upload extends Zend_Form
{

    public function init()
    {
        $folderReject = WWW_ROOT . DIRECTORY_SEPARATOR . 'pictures';
        $this->setName('upload');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttribs(array('class' => 'btn btn-info'))
                ->setLabel('Upload');
        $this->addElements(array($submit));
    }


}
