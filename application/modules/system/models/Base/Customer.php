<?php

/**
 * System_Model_Base_Customer
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $name
 * @property string $code
 * @property string $remarks
 * @property Doctrine_Collection $System_Model_MainReport
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class System_Model_Base_Customer extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('pe_customer');
        $this->hasColumn('name', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'unique' => true,
             'length' => '50',
             ));
        $this->hasColumn('code', 'string', 4, array(
             'type' => 'string',
             'notnull' => true,
             'unique' => true,
             'length' => '4',
             ));
        $this->hasColumn('remarks', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('System_Model_MainReport', array(
             'local' => 'code',
             'foreign' => 'customer_code'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $updateable0 = new Doctrine_Template_Updateable();
        $this->actAs($timestampable0);
        $this->actAs($updateable0);
    }
}