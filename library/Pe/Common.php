<?php

class Pe_Common
{
    static public function existingValue($model, array $values, $excById = false)
    {
        $sql = Doctrine_Query::create()->from($model);
        $numberOfRecord = 0;
        if(count($values)):
            foreach($values as $k => $v) $sql->andWhere($k . ' = ?', $v);
            if($excById) $sql->andWhere('id <> ?', $excById);
            $sql = $sql->execute();
            $numberOfRecord = count($sql);
        endif;
        return $numberOfRecord;
    }

    static public function rejectConvertionByCode()
    {
        $sql = Doctrine_Query::create()
             ->from('System_Model_Reject')
             ->orderBy('code')->execute();
        $li0 = array();
        if(count($sql))
            foreach($sql as $r)
                $li0[$r->code] = array('id' => $r->id, 'name' => $r->name,
                    'code' => $r->code, 'area' => $r->area_code);
        return $li0;
    }

    static public function chartSourceGeneralConverter($data, $limit = false)
    {
        $source = array();
        if(is_array($data) && count($data)):
            $countValues = array_count_values($data);
            arsort($countValues);
            if($limit == false) $limit = count($countValues);
            $source0 = array_slice($countValues, 0, $limit);
            foreach($source0 as $k => $v) $source[] = array($k, $v);
        endif;
        return $source;
    }

    static public function wwList($display = 16, $dayZero = 0, $date = false)
    {
        $d0 = new Zend_Date();
        if($date) $d0 = new Zend_Date($date, Zend_Date::ISO_8601);
        $numberOfTheDay = $d0->toString('e');
        $currentWw      = $d0->toString('w');
        $firstDayOfWW   = $d0->subDay($numberOfTheDay);
        $ww = array();
        for($i = 0; $i < $display; $i++):
            $clone = clone $firstDayOfWW;
            $prevWw = $clone->subDay($i * 7);
            $ww[$prevWw->toString('yyyyww')] =
                array('start' => $prevWw->toString('yyyy-MM-dd'),
                      'end' => $prevWw->addDay(7)->toString('yyyy-MM-dd')
                  );
        endfor;
        return $ww;
    }
}
