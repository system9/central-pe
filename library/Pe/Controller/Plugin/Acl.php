<?php

/**
 * Description of Auth
 *
 * @author heaven-dragon
 */
class Pe_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $auth = Zend_Auth::getInstance();
        $role = 'public';
        if($auth->hasIdentity()) $role = strtolower($auth->getIdentity()->role);

        $acl = new Zend_Acl();
        $acl->addRole(new Zend_Acl_Role('public'))
                ->addRole(new Zend_Acl_Role('pe'), 'public')
                ->addRole(new Zend_Acl_Role('system-admin'));

        $acl->addResource(new Zend_Acl_Resource('dashboard'))
                ->addResource(new Zend_Acl_Resource('system'))
                ->addResource(new Zend_Acl_Resource('admin'));

        $resource = $request->getModuleName();
        $privilege = $request->getControllerName();
        $acl->deny();
        $acl->allow('public', array('dashboard'), array('index', 'error', 'install', 'login', 'api'));
        $acl->allow(array('pe'), 'dashboard', array('shiftly', 'profile'));
        $acl->allow(array('system-admin'));

        if(!$acl->isAllowed($role, $resource, $privilege)):
            $request->setModuleName('dashboard');
            if('public' == $role):
                $request->setControllerName('login')->setActionName('index');
            else:
                $request->setControllerName('index')->setActionName('index');
            endif;
                $request->setDispatched(true);
        endif;

        if(!$acl->has($resource)){
            throw new Exception($resource . ' was not on the resource list');
        }


    }
}
