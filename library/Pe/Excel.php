<?php

class Pe_Excel
{
    protected $_excel;
    protected $_title;
    protected $_prefixTitle = '';
    protected $_firstRow;
    protected $_actual;

    public function setTitle($title)
    {
        if($title) $this->_title = $title;
        return $this;
    }
    public function setFirstRow($row)
    {
        if($row) $this->_firstRow = $row;
        return $this;
    }

    public function setActual($actual, $status = true)
    {
        if($actual)$this->_actual = $actual;
        return $this;
    }

    public function __construct($filename = false)
    {
        $this->_excel = new PHPExcel();
        if($filename && is_readable($filename))
            $this->_excel = PHPExcel_IOFactory::load($filename);
        $this->_excel->setActiveSheetIndex(0);
    }


    public function general($list)
    {
        if(count($list)):
            $ws0 = $this->_excel->getActiveSheet();
            $ws0->setTitle('General Report');
            $ws0->getCell('A1')->setValue('General Report');
            $ws0->mergeCells('A1:K1');

            $ws0->getStyle('A1:K1')->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                      ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $h0  = 5;
            if($this->_firstRow) $h0 = $this->_firstRow;
            $ct0 = $h0 + 1;
            $ws0->getCell('A' . $h0)->setValue('No');
            $ws0->getCell('B' . $h0)->setValue('Lot');
            $ws0->getCell('C' . $h0)->setValue('Package');
            $ws0->getCell('D' . $h0)->setValue('Customer');
            $ws0->getCell('E' . $h0)->setValue('Problem');
            $ws0->getCell('F' . $h0)->setValue('Problem Type');
            $ws0->getCell('G' . $h0)->setValue('Area');
            $ws0->getCell('H' . $h0)->setValue('Mc Number');
            $ws0->getCell('I' . $h0)->setValue('#Reject');
            $ws0->getCell('J' . $h0)->setValue('Created At');
            $ws0->getCell('K' . $h0)->setValue('Created By');
            $i = 1;
            foreach($list as $k => $m0):
                if(count($m0['report'])):
                    foreach($m0['report'] as $k => $r):
                        $ws0->getCell('A' . $ct0)->setValue($i);
                        $ws0->getCell('B' . $ct0)->setValue($m0['lot']);
                        $ws0->getCell('C' . $ct0)->setValue($m0['package']);
                        $ws0->getCell('D' . $ct0)->setValue($m0['customer']);
                        $ws0->getCell('E' . $ct0)->setValue($r['reject-name']);
                        $ws0->getCell('F' . $ct0)->setValue($r['problem-type']);
                        $ws0->getCell('G' . $ct0)->setValue($r['area']);
                        $ws0->getCell('H' . $ct0)->setValue($r['area'] . '-' . $r['mc-number']);
                        $ws0->getCell('I' . $ct0)->setValue($r['reject-qty']);
                        $ws0->getCell('J' . $ct0)->setValue($r['created-at']);
                        $ws0->getCell('K' . $ct0)->setValue($r['created-by']);
                        if($ct0 % 2 != 0) $ws0->getStyle('A' . $ct0 . ':K' . $ct0)
                                            ->applyFromArray($this->strippedStyle());
                        $i++;
                        $ct0++;
                    endforeach;
                endif;
            endforeach;
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $title = 'General Report';
            if($this->_title) $title = $this->_title;
            header('Content-Disposition: attachment;filename="' . $title . '.xlsx"');

            $objWriter = PHPExcel_IOFactory::createWriter($this->_excel, 'Excel2007');
            ob_end_clean();

            $objWriter->save('php://output');
            $this->_excel->disconnectWorksheets();
            unset($this->_excel);
        endif;
    }



    public function headerStyle()
    {
        return array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN)));
    }

    public function borderStyle()
    {
        return array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'C0C0C0')),
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'C0C0C0')),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'C0C0C0')),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'C0C0C0'))
            )
        );
    }

    public function strippedStyle()
    {
        return array(
            'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('argb' => 'D9D9D9'))
        );
    }
}
