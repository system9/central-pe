<?php

class Pe_User
{
    protected $_user;
    public function __construct()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()) $this->_user = $auth->getIdentity();
    }

    public function properties()
    {
        $user = array();
        if($this->_user):
            $sql = Doctrine_Query::create()
                 ->select('u.*, ua.*, up.*')
                 ->from('System_Model_User u')
                 ->leftJoin('u.System_Model_UserAssignment ua')
                 ->leftJoin('u.System_Model_UserPackage up')
                 ->Where('u.username = ?', $this->_user->username)
                 ->execute();
            if(count($sql)):
                foreach($sql as $s):
                    $assignment = array();
                    if(count($s->System_Model_UserAssignment)):
                        foreach($s->System_Model_UserAssignment as $ua):
                            $assignment[] = $ua->area_code;
                        endforeach;
                    endif;
                    $groupPackages = array();
                    if(count($s->System_Model_UserPackage)):
                        foreach($s->System_Model_UserPackage as $up):
                            $groupPackages[] = $up->group_name;
                        endforeach;
                    endif;
                    $user['id']         = $s->id;
                    $user['username']   = $s->username;
                    $user['fullname']   = $s->fullname;
                    $user['role']       = $s->role;
                    $user['shift']      = $s->shift;
                    $user['assignment'] = $assignment;
                    $user['group-packages'] = $groupPackages;
                    $user['reject-list']    = array();
                    $user['package-list']   = array();
                endforeach;
                if(count($user['assignment'])):
                    $rjc = Doctrine_Query::create()
                         ->from('System_Model_Reject')
                         ->andWhereIn('area_code', $user['assignment'])
                         ->orderBy('area_code, name, code')->execute();
                    $li0 = array();
                    if(count($rjc))
                        foreach($rjc as $r)
                            $li0[$r->area_code][] = array('id' => $r->id, 'name' => $r->name,
                                'code' => $r->code, 'area' => $r->area_code);
                    $user['reject-list'] = $li0;
                endif;
                if(count($user['group-packages'])):
                    $pkgs = Doctrine_Query::create()
                         ->select('g.*, p.*')
                         ->from('System_Model_PackageGroup g')
                         ->leftJoin('g.System_Model_Package p')
                         ->whereIn('g.name', $user['group-packages'])
                         ->orderBy('g.name, p.name')
                         ->execute();
                     $pkgGroup = array();
                     if(count($pkgs)):
                         foreach($pkgs as $g):
                             $packageList = array();
                             if(count($g->System_Model_Package)):
                                 foreach($g->System_Model_Package as $p)
                                     $packageList[] = array('name' => $p->name, 'id' => $p->id);
                             endif;
                             $pkgGroup[$g->name] = array('id' => $g->id, 'name' => $g->name, 'packages' => $packageList);
                         endforeach;
                     endif;
                     $user['package-list'] = $pkgGroup;
                endif;
            endif;
        endif;
        return $user;
    }
}
