'use strict';

function generalChart(chartId, source) {
    var plot1 = $.jqplot(chartId, [source], {
        // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
        animate: !$.jqplot.use_excanvas,
        seriesDefaults: {
            renderer: $.jqplot.BarRenderer,
            pointLabels: {
                show: true
            }
        },
        axes: {
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                label: 'Package'
            },
            yaxis: {
                pad: 1.5,
                label: 'Occurence',
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
            }
        },
        highlighter: {
            show: false
        }
    });
}
